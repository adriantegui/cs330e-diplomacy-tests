from unittest import main, TestCase

from Diplomacy import diplomacy_solveBattle, diplomacy_solve, diplomacy_readRound, diplomacy_print, diplomacy_eval
from io import StringIO

class TestDiplomacy (TestCase):

    # --- readRound --- #
    # returns two variables, round and city
    # round is a list of the armies and their objective city
    # city is the unique list of the cities being fought in
    def test_readRound1(self):
        s = "A Madrid Hold\n"
        round, city = diplomacy_readRound(s)
        self.assertEqual(round, ["A Madrid"])
        self.assertEqual(city, ["Madrid"])

    def test_readRound2(self):
        s = "B Barcelona Move Madrid\n"
        round, city = diplomacy_readRound(s)
        self.assertEqual(round, ["B Madrid"])
        self.assertEqual(city, ["Madrid"])

    def test_solve1(self):
        a = diplomacy_solve("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        self.assertEqual(a, ["A [dead]", "B Madrid", "C London"])

    def test_solve2(self):
        a = diplomacy_solve("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        self.assertEqual(a, ["A [dead]", "B [dead]", "C [dead]", "D Paris", "E Austin"])

    def test_solve3(self):
        a = diplomacy_solve("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
        self.assertEqual(a, ["A [dead]", "B [dead]", "C [dead]"])
    def test_solve4(self):
        a = diplomacy_solve("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B")
        self.assertEqual(a, ["A [dead]", "B Madrid", "C [dead]", "D Paris"])
    def test_solve5(self):
        a = diplomacy_solve("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")
        self.assertEqual(a, ["A [dead]","B [dead]","C [dead]", "D Paris","E Austin"])

    def test_eval1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
        w = StringIO()
        diplomacy_eval(r,w)
        self.assertEqual(w.getvalue(),"A [dead]\nB [dead]\nC [dead]\n")


    def test_print1(self):
        w = StringIO()
        results = ["A [dead]", "B Madrid", "C London"]
        diplomacy_print(w,results)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")
    def test_print2(self):
        w = StringIO()
        results = ["A [dead]", "B [dead]", "C [dead]", "D Paris", "E Austin"]
        diplomacy_print(w,results)
        self.assertEqual(w.getvalue(),"A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")
# ----
# main
# ----
if __name__ == "__main__":
    main()
