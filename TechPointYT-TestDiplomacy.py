# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase
from Diplomacy import diplomacy_eval, diplomacy_print, diplomacy_solve
import War
import Army


# -----------
# TestDiplomacy
# -----------

class TestCollatz (TestCase):
    # ----
    # eval
    # ----

    def test_eval(self):
        vals = ["A Madrid Hold"]
        result = diplomacy_eval(vals)
        self.assertEqual(result, ["A Madrid"])

    def test_eval2(self):
        vals = ["A Madrid Hold", "B Barcelona Move Madrid"]
        result = diplomacy_eval(vals)
        self.assertEqual(result, ["A [dead]","B [dead]"])


    def test_eval3(self):
        vals = ["A Madrid Hold", "B Barcelona Move Madrid","C London Support B"]
        result = diplomacy_eval(vals)
        self.assertEqual(result, ["A [dead]", "B Madrid", "C London"])
    def test_eval4(self):
        vals = ["A Madrid Hold", "B Barcelona Move Madrid","C London Support A"]
        result = diplomacy_eval(vals)
        self.assertEqual(result, ["A Madrid", "B [dead]", "C London"])

    # ----
    # solve
    # ----

    def test_solve(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual( w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")
    def test_solve2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")
    def test_solve3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Support A\nC London Support B\nD Paris Support E\nE Austin Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Barcelona\nC London\nD Paris\nE [dead]\n")
    def test_solve4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Support C\nE Dallas Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC London\nD Austin\nE Dallas\n")
    def test_solve5(self):
        r = StringIO("A Madrid Hold\nB Barcelona Support A\nC London Support B\nD Paris Move Barcelona")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB Barcelona\nC London\nD [dead]\n")

    # ----
    # print
    # ----

    def test_print(self):
        armies = ["A [dead]"]
        w = StringIO()
        diplomacy_print(w, armies)
        self.assertEqual(w.getvalue(), "A [dead]\n")
    def test_print2(self):
        armies = ["A [dead]", "B London"]
        w = StringIO()
        diplomacy_print(w, armies)
        self.assertEqual(w.getvalue(), "A [dead]\nB London\n")
    def test_print3(self):
        armies = ["A [dead]", "B London","C [dead]"]
        w = StringIO()
        diplomacy_print(w, armies)
        self.assertEqual(w.getvalue(), "A [dead]\nB London\nC [dead]\n")
    def test_print3(self):
        armies = ["A [dead]", "C [dead]","B London"]
        w = StringIO()
        diplomacy_print(w, armies)
        self.assertEqual(w.getvalue(), "A [dead]\nB London\nC [dead]\n")

    # ----
    # main
    # ---- pragma : no cover
if __name__ == "__main__":# pragma: no cover
    main()#pragma : no cover

""" #pragma: no cover

............
----------------------------------------------------------------------
Ran 12 tests in 0.002s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Army.py               15      0      0      0   100%
City.py                5      0      0      0   100%
Diplomacy.py          14      0      4      0   100%
Result.py             17      0      6      0   100%
TestDiplomacy.py      62      0      0      0   100%
War.py                40      0     12      0   100%
WarMap.py              8      0      2      0   100%
--------------------------------------------------------------
TOTAL                161      0     24      0   100%


"""