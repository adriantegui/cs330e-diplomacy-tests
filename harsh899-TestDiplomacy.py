#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

# -----------
# TestCollatz
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "A Madrid Hold\n"
        line = diplomacy_read(s)
        self.assertEqual(line, ['A', 'Madrid', 'Hold'])
        
    def test_read_2(self):
        s = "B Barcelona Move Madrid\n"
        line = diplomacy_read(s)
        self.assertEqual(line, ['B', 'Barcelona', 'Move','Madrid'])
        
    def test_read_3(self):
        s = "C London Support A\n"
        line = diplomacy_read(s)
        self.assertEqual(line, ['C', 'London', 'Support','A'])


    # ----
    # eval
    # ----

    def test_eval_1(self):
        sup, move = diplomacy_eval([['A', 'Madrid', 'Hold']])
        self.assertEqual(sup['A'],0)
        self.assertEqual(move['Madrid'], ['A'])

    def test_eval_2(self):
        sup, move = diplomacy_eval([['A', 'Madrid', 'Hold'],['B', 'Barcelona', 'Move','Madrid'],['C', 'London', 'Support','A']])
        self.assertEqual(sup['A'],1)
        self.assertEqual(sup['B'],0)
        self.assertEqual(sup['C'],0)
        self.assertEqual(move['Madrid'], ['A'])
        self.assertEqual(move['Barcelona'],[])
        self.assertEqual(move['London'],['C'])

    def test_eval_3(self):
        sup, move = diplomacy_eval([['A', 'Madrid', 'Hold'],['B', 'Barcelona', 'Move','Madrid']])
        self.assertEqual(sup['A'],0)
        self.assertEqual(sup['B'],0)
        self.assertEqual(move['Madrid'], ['dead'])
        self.assertEqual(move['Barcelona'], [])

    def test_eval_4(self):
        sup, move = diplomacy_eval([['A', 'Madrid', 'Hold'],['B', 'Barcelona', 'Move','Madrid'],['C', 'London', 'Support','B'],['D','Austin','Move','London']])
        self.assertEqual(sup['A'],0)
        self.assertEqual(sup['B'],0)
        self.assertEqual(sup['C'],0)
        self.assertEqual(sup['D'],0)
        self.assertEqual(move['Madrid'], ['dead'])
        self.assertEqual(move['Barcelona'],[])
        self.assertEqual(move['London'], ['dead'])
        self.assertEqual(move['Austin'],[])

    def test_eval_5(self):
        sup, move = diplomacy_eval([['A', 'Madrid', 'Hold'],['B', 'Barcelona', 'Move','Madrid'],['C', 'London', 'Move','Madrid']])
        self.assertEqual(sup['A'],0)
        self.assertEqual(sup['B'],0)
        self.assertEqual(sup['C'],0)             
        self.assertEqual(move['Madrid'], ['dead'])
        self.assertEqual(move['Barcelona'], [])
        self.assertEqual(move['London'], [])

    def test_eval_6(self):
        sup, move = diplomacy_eval([['A', 'Madrid', 'Hold'],['B', 'Barcelona', 'Move','Madrid'],['C', 'London', 'Move','Madrid'],['D', 'Paris','Support','B']])
        self.assertEqual(sup['A'],0)
        self.assertEqual(sup['B'],1)
        self.assertEqual(sup['C'],0)
        self.assertEqual(sup['D'],0)
        self.assertEqual(move['Madrid'], ['B'])
        self.assertEqual(move['Barcelona'], [])
        self.assertEqual(move['London'], [])
        self.assertEqual(move['Paris'], ['D'])
        
    def test_eval_7(self):
        sup, move = diplomacy_eval([['A', 'Madrid', 'Hold'],['B', 'Barcelona', 'Move','Madrid'],['C', 'London', 'Move','Madrid'],['D', 'Paris','Support','B'],['E', 'Austin', 'Support','A']])
        self.assertEqual(sup['A'],1)
        self.assertEqual(sup['B'],1)
        self.assertEqual(sup['C'],0)
        self.assertEqual(sup['D'],0)
        self.assertEqual(sup['E'],0)
        self.assertEqual(move['Madrid'], ['dead'])
        self.assertEqual(move['Barcelona'], [])
        self.assertEqual(move['London'], [])
        self.assertEqual(move['Paris'], ['D'])
        self.assertEqual(move['Austin'], ['E'])

    # -----
    # print
    # -----

    def test_print_1(self):
        w = StringIO()
        diplomacy_print({'A':0},{'Madrid':['A']},w)
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print({'A':1, 'B':0,'C':0},{'Madrid':['A'],'Barcelona':[],'London':['C']},w)
        self.assertEqual(w.getvalue(), "A Madrid\nB [dead]\nC London\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print({'A':0, 'B':0},{'Madrid':['dead'],'Barcelona':[]},w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    def test_print_4(self):
        w = StringIO()
        diplomacy_print({'A':0, 'B':0,'C':0,'D':0},{'Madrid':['dead'],'Barcelona':[],'London':['dead'],'Austin':[]},w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_print_5(self):
        w = StringIO()
        diplomacy_print({'A':0, 'B':0,'C':0},{'Madrid':['dead'],'Barcelona':[],'London':[]},w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_print_6(self):
        w = StringIO()
        diplomacy_print({'A':0, 'B':1,'C':0,'D':0},{'Madrid':['B'],'Barcelona':[],'London':[],'Paris':['D']},w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")
        
    def test_print_7(self):
        w = StringIO()
        diplomacy_print({'A':1, 'B':1,'C':0,'D':0,'E':0},{'Madrid':['dead'],'Barcelona':[],'London':[],'Paris':['D'],'Austin':['E']},w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            str(w.getvalue()), "A [dead]\nB Madrid\nC London\n")

    def test_solve_2(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            str(w.getvalue()), "A [dead]\nB [dead]\nC London\nD Austin\n")
        
    def test_solve_3(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            str(w.getvalue()), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_4(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support A\nD Austin Support A\nE Egypt Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            str(w.getvalue()), "A Madrid\nB [dead]\nC [dead]\nD Austin\nE [dead]\n")

    def test_solve_5(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support A\nD Austin Support A\nE Egypt Move London\nF France Move Austin\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            str(w.getvalue()), "A [dead]\nB [dead]\nC [dead]\nD [dead]\nE [dead]\nF [dead]\n")

# ----
# main
# ----


if __name__ == "__main__":
    main()

