#need other two tests
from io import StringIO
from unittest import main, TestCase
from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

class TestDiplomacy (TestCase):

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(),"A [dead]\nB Madrid\nC London\n")
    
    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB London Move Madrid\nC Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(),"A [dead]\nB [dead]\nC London\n")
    
    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(
            w.getvalue(),"A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
    

if __name__ =="__main__":
    main()
