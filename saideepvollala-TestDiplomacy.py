#!/usr/bin/env python3

# -------------------------------
# projects/Diplomacy/TestDiplomacy.py
# Saideep Vollala
# Samraz Badruddin
# -------------------------------

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_solve

# -----------
# TestDiplomacy
# -----------

class TestDiplomacy (TestCase):
    # ----
    # read functions
    # ----
    def test_read_1(self):
        s = "C Houston Move LA"
        i, j, k, l = diplomacy_read(s)
        self.assertEqual(i, "C")
        self.assertEqual(j, "Houston")
        self.assertEqual(k, "Move")
        self.assertEqual(l, "LA")
      
    def test_read_2(self):
        s = "A Madrid Hold"
        i, j, k = diplomacy_read(s)
        self.assertEqual(i,  "A")
        self.assertEqual(j, "Madrid")
        self.assertEqual(k, "Hold")
    # ----
    # solve functions
    # ----
    
    def test_solve_1(self):
        r = StringIO("A Perth Hold\nB Houston Move NewYork\nC Dallas Support A\nD Denver Hold\nE Chicago Move NewYork\nF LA Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Perth\nB [dead]\nC Dallas\nD Denver\nE [dead]\nF LA")
    
    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London")
     
    def test_solve_3(self):
        r = StringIO("A Tokyo Move Beijing")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Beijing")

    def test_solve_4(self):
        r = StringIO("A HongKong Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A HongKong")
    
    def test_solve_5(self):
        r = StringIO("A Boston Hold\nB NY Move Boston\nC Miami Support B\nD Houston Move Miami")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]")
          
if __name__ == "__main__":
    main()
    
    
""" #pragma: no cover
% coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


% cat TestDiplomacy.out
......
----------------------------------------------------------------------
Ran 6 tests in 0.004s

OK

......
----------------------------------------------------------------------
Ran 6 tests in 0.004s

OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py         106      8     60      5    92%   80-82, 108-110, 135-136, 37->32, 79->80, 93->87, 107->108, 134->135
TestDiplomacy.py      39      0      0      0   100%
--------------------------------------------------------------
TOTAL                145      8     60      5    94%
               204      0     34      0   100%


"""
