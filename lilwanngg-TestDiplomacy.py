#!/usr/bin/env python3

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_1(self):
        inputString = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support B')
        output = StringIO()
        outputStr = 'A [dead]\nB Madrid\nC London\n'
        diplomacy_solve(inputString, output)
        self.assertEqual(output.getvalue(), outputStr)

    def test_2(self):
        inputString = StringIO('A Houston Hold\nB Seattle Move NewYork\nC Denver Support B\nD NewYork Support G\nE Midland Move Houston\nF Paris Hold\nG Austin Move Paris')
        output = StringIO()
        outputStr = 'A [dead]\nB NewYork\nC Denver\nD [dead]\nE [dead]\nF [dead]\nG [dead]\n'
        diplomacy_solve(inputString, output)
        self.assertEqual(output.getvalue(), outputStr)

    def test_3(self):
        inputString = StringIO('A Barcelona Hold\nB Madrid Hold\nC Nice Move Barcelona\nD London Support C\nE Lisbon Support A\nF Houston Move Lisbon')
        output = StringIO()
        outputStr = 'A [dead]\nB Madrid\nC Barcelona\nD London\nE [dead]\nF [dead]\n'
        diplomacy_solve(inputString, output)
        self.assertEqual(output.getvalue(), outputStr)

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover"""
