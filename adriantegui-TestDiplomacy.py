#!/usr/bin/env python3

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve

class TestDiplomacy(TestCase):
    
    
    def test_read(self):
        s = "A Madrid Hold"
        a, b, c = diplomacy_read(s)
        self.assertEqual(a, "A")
        self.assertEqual(b, "Madrid")
        self.assertEqual(c, "Hold")
    
    def test_solve_1(self):
        r = StringIO("A Madrid Hold")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\n")
        
    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Dallas Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\nD Dallas\n")
        
    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")
        
    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
    
    def test_solve_5(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")
        
    def test_solve_6(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")
        
    def test_solve_7(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")
        
    def test_solve_8(self):
        r = StringIO("A Madrid Hold\nB Barcelona Support A\nC London Support B\nD Paris Move Barcelona")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB Barcelona\nC London\nD [dead]\n")
        
    def test_solve_9(self):
        r = StringIO("A Austin Hold\nB Dallas Move Austin\nC Houston Move SanAntonio")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC SanAntonio\n")
        
    def test_solve_10(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Barcelona")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC Barcelona\n")
        
    def test_solve_11(self):
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Move Madrid")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Barcelona\nB Madrid\n")
        
        
if __name__ == "__main__":
    main()
