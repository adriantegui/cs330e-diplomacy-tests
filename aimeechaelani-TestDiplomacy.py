# Write unit tests in TestDiplomacy.py that test corner cases and failure cases until
# you have an 3 tests for the function diplomacy_solve(), confirm the expected failures,
# and add, commit, and push to the private code repo.

#!/usr/bin/env python3

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_solve, diplomacy_print, diplomacy_eval

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):

    # ----
    # read
    # ----

    # diplomacy_read takes the input army, reads all the initial conditions, and makes a list of the initial army conditions

    def test_read_1(self):
        file = "A Madrid Move Barcelona\n"
        a = diplomacy_read(file)
        self.assertEqual(a, ["A", "Madrid", "Move", "Barcelona"])

    def test_read_2(self):
        file = "B Barcelona Hold\n"
        a = diplomacy_read(file)
        self.assertEqual(a, ["B", "Barcelona", "Hold"])

    def test_read_3(self):
        file = "C London Support B\n"
        a = diplomacy_read(file)
        self.assertEqual(a, ["C", "London", "Support", "B"])

    def test_read_4(self):
        file = "D Austin Move London\n"
        a = diplomacy_read(file)
        self.assertEqual(a, ["D", "Austin", "Move", "London"])

    # -----
    # solve
    # -----

    # diplomacy_solve takes the list of initial army conditions and finds their final states

    def test_solve_1(self):
        a = [["A", "Madrid", "Move", "Barcelona"], ["B", "Barcelona", "Hold"], [
            "C", "London", "Support", "B"], ["D", "Austin", "Move", "London"]]
        v = diplomacy_solve(a)
        self.assertEqual(
            v, {"A": ["[dead]"], "B": ["[dead]"], "C": ["[dead]"], "D": ["[dead]"]})

    def test_solve_2(self):
        a = [["A", "Madrid", "Support", "B"], [
            "B", "Barcelona", "Move", "London"], ["C", "London", "Hold"]]
        v = diplomacy_solve(a)
        self.assertEqual(v, {"A": ["Madrid"], "B": [
                         "London"], "C": ["[dead]"]})

    def test_solve_3(self):
        a = [["A", "Madrid", "Support", "B"], ["B", "Barcelona", "Move", "London"], [
            "C", "London", "Hold"], ["D", "Austin", "Support", "C"]]
        v = diplomacy_solve(a)
        self.assertEqual(
            v, {"A": ["Madrid"], "B": ["[dead]"], "C": ["[dead]"], "D": ["Austin"]})

    def test_solve_4(self):
        a = [["A", "Madrid", "Move", "Dallas"], ["B", "Barcelona",
                                                    "Move", "London"], ["C", "London", "Move", "Madrid"]]
        v = diplomacy_solve(a)
        self.assertEqual(v, {"A": ["Dallas"], "B": [
                         "London"], "C": ["Madrid"]})

    # -----
    # print
    # -----

    # diplomacy_print takes the dictionary of final army states and prints them in the correct format

    def test_print_1(self):
        w = StringIO()
        diplomacy_print(
            w, {"A": ["[dead]"], "B": ["[dead]"], "C": ["[dead]"], "D": ["[dead]"]})
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_print_2(self):
        w = StringIO()
        diplomacy_print(w, {"A": ["Madrid"], "B": ["London"], "C": ["[dead]"]})
        self.assertEqual(w.getvalue(), "A Madrid\nB London\nC [dead]\n")

    def test_print_3(self):
        w = StringIO()
        diplomacy_print(
            w, {"A": ["Madrid"], "B": ["[dead]"], "C": ["[dead]"], "D": ["Austin"]})
        self.assertEqual(
            w.getvalue(), "A Madrid\nB [dead]\nC [dead]\nD Austin\n")

    def test_print_4(self):
        w = StringIO()
        diplomacy_print(w, {"A": ["Barcelona"], "B": [
                        "London"], "C": ["Madrid"]})
        self.assertEqual(w.getvalue(), "A Barcelona\nB London\nC Madrid\n")

    # ----
    # eval
    # ----

    # diplomacy_eval takes the string of input armies and returns their final states

    def test_eval_1(self):
        r = StringIO(
            "A Madrid Move Barcelona\nB Barcelona Hold\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_eval(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_eval_2(self):
        r = StringIO(
            "A Madrid Support B\nB Barcelona Move London\nC London Hold\n")
        w = StringIO()
        diplomacy_eval(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB London\nC [dead]\n")

    def test_eval_3(self):
        r = StringIO(
            "A Madrid Support B\nB Barcelona Move London\nC London Hold\nD Austin Support C\n")
        w = StringIO()
        diplomacy_eval(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB [dead]\nC [dead]\nD Austin\n")

    def test_eval_4(self):
        r = StringIO(
            "A Madrid Move Barcelona\nB Barcelona Move London\nC London Move Madrid\n")
        w = StringIO()
        diplomacy_eval(r, w)
        self.assertEqual(
            w.getvalue(), "A Barcelona\nB London\nC Madrid\n")

# ----
# main
# ----


if __name__ == "__main__":
    main()
