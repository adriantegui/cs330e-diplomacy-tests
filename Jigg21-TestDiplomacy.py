import unittest
from io import StringIO
from Diplomacy import diplomacy_solve

class TestDiplomacy(unittest.TestCase):

    def test_1(self):
        inputString = StringIO("A Madrid Move London\nB London Move Madrid")
        outputObject = StringIO()
        outputString = "A London\nB Madrid"
        diplomacy_solve(inputString,outputObject)
        self.assertTrue(outputObject.getvalue().strip() == outputString.strip())

    def test_2(self):
        inputString = StringIO("A Paris Hold\n B NewYork Move Paris")
        outputObject = StringIO()
        outputString = "A [dead]\nB [dead]"
        diplomacy_solve(inputString,outputObject)
        self.assertTrue(outputObject.getvalue().strip() == outputString.strip())
    	
    def test_3(self):
        inputString = StringIO("A Madrid Hold\nB London Move Madrid \nC Barcelona Support B\nD Paris Move Barcelona")
        outputObject = StringIO()
        outputString = "A [dead]\nB [dead]\nC [dead]\nD [dead]\n"
        diplomacy_solve(inputString,outputObject)
        self.assertTrue(outputObject.getvalue().strip() == outputString.strip())
    
    def test_4(self):
        inputString = StringIO("A Madrid Hold\nB London Move Madrid \nC Barcelona Support B")
        outputObject = StringIO()
        outputString = "A [dead]\nB Madrid\nC Barcelona\n"
        diplomacy_solve(inputString,outputObject)
        self.assertTrue(outputObject.getvalue().strip() == outputString.strip())
    
    def test_5(self):
        inputString = StringIO("A Madrid Hold\nB London Move Madrid \nC Barcelona Support A")
        outputObject = StringIO()
        outputString = "A Madrid\nB [dead]\nC Barcelona\n"
        diplomacy_solve(inputString,outputObject)
        self.assertTrue(outputObject.getvalue().strip() == outputString.strip())
        
if __name__ == '__main__':
    unittest.main()
