# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_battle, diplomacy_eval, diplomacy_print, diplomacy_solve


# -----------
# TestDiplomacy
# -----------

class TestDiplomacy(TestCase):

	#diplomacy_solve
	def test_solve_1(self):
		r = StringIO('A London Hold\nB Madrid Move Paris\nC Berlin Move Paris\nD Vienna Support B\nE Rome Move Paris\n')
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(
			w.getvalue(), "A London\nB Paris\nC [dead]\nD Vienna\nE [dead]\n")

	def test_solve_2(self):
		r = StringIO('A London Move Paris\nB Madrid Hold\nC Berlin Move Paris\
					\nD Vienna Support C\nE Rome Support A\nF Athens Support C\n')
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(
			w.getvalue(), "A [dead]\nB Madrid\nC Paris\nD Vienna\nE Rome\nF Athens\n")

	def test_solve_3(self):
		r = StringIO('A London Move Paris\nB Madrid Hold\nC Berlin Move Paris\
					\nD Vienna Support C\nE Rome Move Vienna\nF Athens Support A\n')
		w = StringIO()
		diplomacy_solve(r, w)
		self.assertEqual(
			w.getvalue(), "A Paris\nB Madrid\nC [dead]\nD [dead]\nE [dead]\nF Athens\n")


if __name__ == "__main__":
	main()

""" #pragma: no cover
% coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


% cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestDiplomacy.out



% cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Diplomacy.py          12      0      2      0   100%
TestDiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""