from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve


class TestDiplomacy(TestCase):

    # ----
    # solve
    # ----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_solve_5(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

    def test_solve_6(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    def test_solve_7(self):
        r = StringIO("A Paris Move Barcelona\nB Barcelona Support C\nC Berlin Move Rome\nD Rome Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    # Edge case: All armies move separate locations
    def test_solve_8 (self):
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Move London\nC London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Barcelona\nB London\nC Madrid\n")

    # # # Edge case: Army Move to unestablished location
    def test_solve_9 (self):
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Barcelona\nB London\n")

    # # # Edge case: two armies move to same location
    def test_solve_10 (self):
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Move London\nC London Move Barcelona\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB London\nC [dead]\n")

    # # # Edge case: Army Move command before Hold
    def test_solve_11 (self):
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")

    # # Edge case: two supporting before army established
    def test_solve_12 (self):
        r = StringIO("A Madrid Support B\nB Barcelona Move London\nC London Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB London\nC [dead]\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()
